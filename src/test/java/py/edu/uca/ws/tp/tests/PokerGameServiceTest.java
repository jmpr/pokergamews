package py.edu.uca.ws.tp.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import py.edu.uca.ws.pokergame.DuplicateUsernameException;
import py.edu.uca.ws.pokergame.Estimation;
import py.edu.uca.ws.pokergame.InvalidDescriptionException;
import py.edu.uca.ws.pokergame.InvalidEstimationException;
import py.edu.uca.ws.pokergame.InvalidGameException;
import py.edu.uca.ws.pokergame.InvalidGameStatusException;
import py.edu.uca.ws.pokergame.InvalidTokenException;
import py.edu.uca.ws.pokergame.InvalidUsernameException;
import py.edu.uca.ws.pokergame.PokerGame;
import py.edu.uca.ws.pokergame.PokerGameResult;
import py.edu.uca.ws.pokergame.PokerGameResult.Estimations;
import py.edu.uca.ws.pokergame.PokerGameResult.Estimations.Entry;
import py.edu.uca.ws.pokergame.PokerGameService;
import py.edu.uca.ws.pokergame.PokerGameState;
import py.edu.uca.ws.pokergame.PokerGameToken;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PokerGameServiceTest {

	private PokerGameService service = new PokerGameService();
	
	private PokerGame pokerGamePort = service.getPokerGamePort();
	
	private static Random random = new Random();
	
	/*
	 * Usados para crear y "jugar" un poker game
	 */
	private final static String OWNER = "haquino";

	private static String description;

	private static String token = null;

	private static String identifier = null;

	@Test(expected = InvalidUsernameException.class)
	public void test01() throws InvalidUsernameException, InvalidDescriptionException {
		// usuario inválido
		pokerGamePort.start("", "una descripción válida");
	}

	@Test(expected = InvalidUsernameException.class)
	public void test02() throws InvalidUsernameException, InvalidDescriptionException {
		// usuario inválido
		pokerGamePort.start(null, "otra descripción válida");
	}

	@Test(expected = InvalidDescriptionException.class)
	public void test03() throws InvalidDescriptionException, InvalidUsernameException {
		// descripción inválida
		pokerGamePort.start("gzacur", "");
	}

	@Test(expected = InvalidDescriptionException.class)
	public void test04() throws InvalidDescriptionException, InvalidUsernameException {
		// descripción inválida
		pokerGamePort.start("gzacur", null);
	}

	@Test(expected = InvalidUsernameException.class)
	public void test05() throws InvalidDescriptionException, InvalidUsernameException {
		// usuario inválido
		pokerGamePort.start(randomLengthEmptyString(), "desde test05");
	}

	@Test(expected = InvalidDescriptionException.class)
	public void test06() throws InvalidDescriptionException, InvalidUsernameException {
		// descripción inválida
		pokerGamePort.start("haquino", randomLengthEmptyString());
	}

	private String randomLengthEmptyString() {
		int n = random.nextInt(99) + 1;
		return RandomStringUtils.random(n, ' ');
	}

	@Test
	public void test07() throws InvalidDescriptionException, InvalidUsernameException, InvalidGameStatusException, InvalidTokenException {
		// creamos el poker game que vamos a jugar en otros tests
		Date d = new Date();
		description = "Poker Game " + d.getTime();
		PokerGameToken thePokerGame = pokerGamePort.start(OWNER, description);

		// verificamos el estado del poker game creado
		assertNotNull(thePokerGame);
		assertNotNull(thePokerGame.getToken());
		assertNotNull(thePokerGame.getIdentifier());
		assertEquals(thePokerGame.getState(), PokerGameState.PLAYING);
		assertEquals(thePokerGame.getOwner(), OWNER);
		assertTrue(thePokerGame.getToken().length() >= 20);
		assertTrue(thePokerGame.getIdentifier().length() >= 8);
		assertNotNull(thePokerGame.getDescription());
		assertEquals(thePokerGame.getDescription(), description);

		// creamos otros n juegos, donde 50 < n < 100
		int n = random.nextInt(50) + 50;
		PokerGameToken[] randomGames = new PokerGameToken[n];
		for (int i = 0; i < n; i++) {
			String randomOwner = RandomStringUtils.random(8, true, false);
			String randomDescription = RandomStringUtils.random(20, true, true);
			PokerGameToken anotherPokerGame = pokerGamePort.start(randomOwner, randomDescription);
			assertNotNull(anotherPokerGame);
			assertEquals(anotherPokerGame.getOwner(), randomOwner);
			assertEquals(anotherPokerGame.getDescription(), randomDescription);
			randomGames[i] = anotherPokerGame;
			for (int j = i - 1; j >= 0; j--) {
				// comparar que no hayan identificadores o tokens repetidos en los juegos creados
				assertNotEquals(anotherPokerGame.getIdentifier(), randomGames[j].getIdentifier());
				assertNotEquals(anotherPokerGame.getToken(), randomGames[j].getToken());
			}
			// comparar con el primero que fue creado
			assertNotEquals(anotherPokerGame.getToken(), thePokerGame.getToken());

			// consultar el estado del juego
			PokerGameResult status = pokerGamePort.status(anotherPokerGame.getToken(), anotherPokerGame.getIdentifier());
			assertEquals(status.getState(), PokerGameState.PLAYING);
			// cerrar el juego
			pokerGamePort.end(anotherPokerGame.getToken(), anotherPokerGame.getIdentifier());
			// volver a consultar el estado
			status = pokerGamePort.status(anotherPokerGame.getToken(), anotherPokerGame.getIdentifier());
			assertEquals(status.getState(), PokerGameState.FINSIHED);
		}

		token = thePokerGame.getToken();
		identifier = thePokerGame.getIdentifier();
	}

	@Test
	public void test08() throws InvalidTokenException {
		// estado del poker game creado en el test anterior
		PokerGameResult status = pokerGamePort.status(token, identifier);
		assertEquals(status.getState(), PokerGameState.PLAYING);
		assertEquals(status.getDescription(), description);
		assertEquals(status.getOwner(), OWNER);
		assertEquals(status.getIdentifier(), identifier);

		// no debe tener estimaciones
		Estimations estimations = status.getEstimations();
		List<Entry> entryList = estimations.getEntry();
		assertTrue(entryList == null || entryList.size() == 0);
	}

	@Test
	public void test09() throws InvalidTokenException, DuplicateUsernameException, InvalidEstimationException, InvalidGameException,
			InvalidGameStatusException, InvalidUsernameException {
		String[] users = { RandomStringUtils.random(12, true, false), RandomStringUtils.random(12, true, false),
				RandomStringUtils.random(12, true, false) };
		Estimation[] estimations = new Estimation[3];
		// 1ra estimación
		estimations[0] = new Estimation();
		estimations[0].setValue(random.nextInt(3) + 1);
		estimations[0].setJustification("Es un ticket muy fácil de solucionar");
		assertEquals(estimations[0].getValue(), pokerGamePort.play(users[0], identifier, estimations[0]));
		// 2da estimación
		estimations[1] = new Estimation();
		estimations[1].setValue(13);
		estimations[1].setJustification("Para mi es complicado");
		assertEquals(estimations[1].getValue(), pokerGamePort.play(users[1], identifier, estimations[1]));
		// 3da estimación
		estimations[2] = new Estimation();
		estimations[2].setValue(5);
		assertEquals(estimations[2].getValue(), pokerGamePort.play(users[2], identifier, estimations[2]));

		// Consultar el estado del juego
		PokerGameResult status = pokerGamePort.status(token, identifier);
		assertEquals(status.getState(), PokerGameState.PLAYING);
		assertEquals(status.getOwner(), OWNER);
		assertEquals(status.getIdentifier(), identifier);

		Estimations returnedEstimations = status.getEstimations();
		List<Entry> list = returnedEstimations.getEntry();
		// tienen que haber 3 estimaciones
		assertTrue(list.size() == 3);

		int i = 0;
		for (Entry entry : list) {
			String user = entry.getKey();
			assertEquals(user, users[i]);
			Estimation estimated = entry.getValue();
			assertEquals(estimated.getValue(), estimations[i].getValue());
			assertEquals(estimated.getJustification(), estimations[i].getJustification());
			i++;
		}
	}

	@Test
	public void test10() throws InvalidTokenException {
		PokerGameResult status = pokerGamePort.status(null, identifier);
		assertEquals(status.getState(), PokerGameState.PLAYING);
		assertEquals(status.getDescription(), description);
		assertEquals(status.getOwner(), OWNER);
		assertEquals(status.getIdentifier(), identifier);

		// no se pasa el token, entonces no debería retornar resultados
		Estimations estimations = status.getEstimations();
		List<Entry> entryList = estimations.getEntry();
		assertTrue(entryList == null || entryList.size() == 0);
	}

	@Test(expected = DuplicateUsernameException.class)
	public void test11() throws DuplicateUsernameException, InvalidEstimationException, InvalidGameException, InvalidGameStatusException,
			InvalidUsernameException {
		// un usuario no puede cambiar su estimación
		Estimation estimation = new Estimation();
		estimation.setValue(50);
		estimation.setJustification("Es un ticket muy fácil de solucionar");
		pokerGamePort.play("mprieto", identifier, estimation);
		estimation.setValue(1);
		estimation.setJustification("Ah no!!!... entendí mal");
		pokerGamePort.play("mprieto", identifier, estimation);
	}

	@Test(expected = InvalidTokenException.class)
	public void test12() throws InvalidGameStatusException, InvalidTokenException {
		// solo con el toker se puede finalizar un juego
		pokerGamePort.end(null, identifier);
	}

	@Test
	public void test13() throws InvalidGameStatusException, InvalidTokenException {
		// se finaliza el juego
		pokerGamePort.end(token, identifier);
		// una vez finalizado el juego, todos pueden consultar. No hace falta tener el token
		PokerGameResult status = pokerGamePort.status(null, identifier);
		assertEquals(status.getState(), PokerGameState.FINSIHED);
		Estimations estimations = status.getEstimations();
		List<Entry> list = estimations.getEntry();
		assertEquals(list.size(), 4);
		for (Entry entry : list) {
			String user = entry.getKey();
			Estimation estimation = entry.getValue();
			System.out.println(String.format("[user : %s ; size : %d ; justification : %s]", user, estimation.getValue(),
					estimation.getJustification()));
		}
	}

	@Test(expected = InvalidGameStatusException.class)
	public void test14() throws DuplicateUsernameException, InvalidEstimationException, InvalidGameException, InvalidGameStatusException, InvalidUsernameException {
		// no se puede realizar una nueva estimación
		Estimation estimation = new Estimation();
		estimation.setValue(1);
		pokerGamePort.play("pepe", identifier, estimation);
	}
	
	@Test(expected = InvalidGameStatusException.class)
	public void test15() throws InvalidGameStatusException, InvalidTokenException {
		// el juego ya está cerrado
		pokerGamePort.end(token, identifier);
	}
	
	@Test(expected = InvalidGameException.class)
	public void test16() throws DuplicateUsernameException, InvalidEstimationException, InvalidGameException, InvalidGameStatusException, InvalidUsernameException {
		// este juego no debería existir
		Estimation estimation = new Estimation();
		estimation.setValue(1);
		pokerGamePort.play("pepe", "-1", estimation);
	}
	
	@Test(expected = InvalidGameStatusException.class)
	public void test17() throws InvalidGameStatusException, InvalidTokenException {
		// esto también debería generar una excepción
		// Cuál de las 2?
		pokerGamePort.end(token, "-1");
	}
}
