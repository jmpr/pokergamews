
package py.edu.uca.ws.pokergame;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the py.edu.uca.ws.pokergame package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Status_QNAME = new QName("http://pokergame.ws.uca.edu.py", "status");
    private final static QName _DuplicateUsername_QNAME = new QName("http://pokergame.ws.uca.edu.py", "DuplicateUsername");
    private final static QName _StatusResponse_QNAME = new QName("http://pokergame.ws.uca.edu.py", "statusResponse");
    private final static QName _InvalidDescription_QNAME = new QName("http://pokergame.ws.uca.edu.py", "InvalidDescription");
    private final static QName _PlayResponse_QNAME = new QName("http://pokergame.ws.uca.edu.py", "playResponse");
    private final static QName _Play_QNAME = new QName("http://pokergame.ws.uca.edu.py", "play");
    private final static QName _StartResponse_QNAME = new QName("http://pokergame.ws.uca.edu.py", "startResponse");
    private final static QName _Start_QNAME = new QName("http://pokergame.ws.uca.edu.py", "start");
    private final static QName _InvalidGameStatus_QNAME = new QName("http://pokergame.ws.uca.edu.py", "InvalidGameStatus");
    private final static QName _InvalidEstimation_QNAME = new QName("http://pokergame.ws.uca.edu.py", "InvalidEstimation");
    private final static QName _InvalidGame_QNAME = new QName("http://pokergame.ws.uca.edu.py", "InvalidGame");
    private final static QName _End_QNAME = new QName("http://pokergame.ws.uca.edu.py", "end");
    private final static QName _InvalidUsername_QNAME = new QName("http://pokergame.ws.uca.edu.py", "InvalidUsername");
    private final static QName _InvalidToken_QNAME = new QName("http://pokergame.ws.uca.edu.py", "InvalidToken");
    private final static QName _EndResponse_QNAME = new QName("http://pokergame.ws.uca.edu.py", "endResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: py.edu.uca.ws.pokergame
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PokerGameResult }
     * 
     */
    public PokerGameResult createPokerGameResult() {
        return new PokerGameResult();
    }

    /**
     * Create an instance of {@link PokerGameResult.Estimations }
     * 
     */
    public PokerGameResult.Estimations createPokerGameResultEstimations() {
        return new PokerGameResult.Estimations();
    }

    /**
     * Create an instance of {@link InvalidDescription }
     * 
     */
    public InvalidDescription createInvalidDescription() {
        return new InvalidDescription();
    }

    /**
     * Create an instance of {@link PlayResponse }
     * 
     */
    public PlayResponse createPlayResponse() {
        return new PlayResponse();
    }

    /**
     * Create an instance of {@link DuplicateUsername }
     * 
     */
    public DuplicateUsername createDuplicateUsername() {
        return new DuplicateUsername();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link StatusResponse }
     * 
     */
    public StatusResponse createStatusResponse() {
        return new StatusResponse();
    }

    /**
     * Create an instance of {@link Play }
     * 
     */
    public Play createPlay() {
        return new Play();
    }

    /**
     * Create an instance of {@link StartResponse }
     * 
     */
    public StartResponse createStartResponse() {
        return new StartResponse();
    }

    /**
     * Create an instance of {@link InvalidEstimation }
     * 
     */
    public InvalidEstimation createInvalidEstimation() {
        return new InvalidEstimation();
    }

    /**
     * Create an instance of {@link InvalidGameStatus }
     * 
     */
    public InvalidGameStatus createInvalidGameStatus() {
        return new InvalidGameStatus();
    }

    /**
     * Create an instance of {@link Start }
     * 
     */
    public Start createStart() {
        return new Start();
    }

    /**
     * Create an instance of {@link EndResponse }
     * 
     */
    public EndResponse createEndResponse() {
        return new EndResponse();
    }

    /**
     * Create an instance of {@link InvalidToken }
     * 
     */
    public InvalidToken createInvalidToken() {
        return new InvalidToken();
    }

    /**
     * Create an instance of {@link InvalidGame }
     * 
     */
    public InvalidGame createInvalidGame() {
        return new InvalidGame();
    }

    /**
     * Create an instance of {@link InvalidUsername }
     * 
     */
    public InvalidUsername createInvalidUsername() {
        return new InvalidUsername();
    }

    /**
     * Create an instance of {@link End }
     * 
     */
    public End createEnd() {
        return new End();
    }

    /**
     * Create an instance of {@link Estimation }
     * 
     */
    public Estimation createEstimation() {
        return new Estimation();
    }

    /**
     * Create an instance of {@link PokerGameInfo }
     * 
     */
    public PokerGameInfo createPokerGameInfo() {
        return new PokerGameInfo();
    }

    /**
     * Create an instance of {@link PokerGameToken }
     * 
     */
    public PokerGameToken createPokerGameToken() {
        return new PokerGameToken();
    }

    /**
     * Create an instance of {@link PokerGameResult.Estimations.Entry }
     * 
     */
    public PokerGameResult.Estimations.Entry createPokerGameResultEstimationsEntry() {
        return new PokerGameResult.Estimations.Entry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Status }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "status")
    public JAXBElement<Status> createStatus(Status value) {
        return new JAXBElement<Status>(_Status_QNAME, Status.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DuplicateUsername }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "DuplicateUsername")
    public JAXBElement<DuplicateUsername> createDuplicateUsername(DuplicateUsername value) {
        return new JAXBElement<DuplicateUsername>(_DuplicateUsername_QNAME, DuplicateUsername.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "statusResponse")
    public JAXBElement<StatusResponse> createStatusResponse(StatusResponse value) {
        return new JAXBElement<StatusResponse>(_StatusResponse_QNAME, StatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidDescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "InvalidDescription")
    public JAXBElement<InvalidDescription> createInvalidDescription(InvalidDescription value) {
        return new JAXBElement<InvalidDescription>(_InvalidDescription_QNAME, InvalidDescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlayResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "playResponse")
    public JAXBElement<PlayResponse> createPlayResponse(PlayResponse value) {
        return new JAXBElement<PlayResponse>(_PlayResponse_QNAME, PlayResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Play }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "play")
    public JAXBElement<Play> createPlay(Play value) {
        return new JAXBElement<Play>(_Play_QNAME, Play.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "startResponse")
    public JAXBElement<StartResponse> createStartResponse(StartResponse value) {
        return new JAXBElement<StartResponse>(_StartResponse_QNAME, StartResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Start }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "start")
    public JAXBElement<Start> createStart(Start value) {
        return new JAXBElement<Start>(_Start_QNAME, Start.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidGameStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "InvalidGameStatus")
    public JAXBElement<InvalidGameStatus> createInvalidGameStatus(InvalidGameStatus value) {
        return new JAXBElement<InvalidGameStatus>(_InvalidGameStatus_QNAME, InvalidGameStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidEstimation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "InvalidEstimation")
    public JAXBElement<InvalidEstimation> createInvalidEstimation(InvalidEstimation value) {
        return new JAXBElement<InvalidEstimation>(_InvalidEstimation_QNAME, InvalidEstimation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidGame }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "InvalidGame")
    public JAXBElement<InvalidGame> createInvalidGame(InvalidGame value) {
        return new JAXBElement<InvalidGame>(_InvalidGame_QNAME, InvalidGame.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link End }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "end")
    public JAXBElement<End> createEnd(End value) {
        return new JAXBElement<End>(_End_QNAME, End.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidUsername }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "InvalidUsername")
    public JAXBElement<InvalidUsername> createInvalidUsername(InvalidUsername value) {
        return new JAXBElement<InvalidUsername>(_InvalidUsername_QNAME, InvalidUsername.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "InvalidToken")
    public JAXBElement<InvalidToken> createInvalidToken(InvalidToken value) {
        return new JAXBElement<InvalidToken>(_InvalidToken_QNAME, InvalidToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EndResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pokergame.ws.uca.edu.py", name = "endResponse")
    public JAXBElement<EndResponse> createEndResponse(EndResponse value) {
        return new JAXBElement<EndResponse>(_EndResponse_QNAME, EndResponse.class, null, value);
    }

}
