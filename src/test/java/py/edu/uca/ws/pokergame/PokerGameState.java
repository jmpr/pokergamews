
package py.edu.uca.ws.pokergame;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pokerGameState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="pokerGameState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PLAYING"/>
 *     &lt;enumeration value="FINSIHED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "pokerGameState")
@XmlEnum
public enum PokerGameState {

    PLAYING,
    FINSIHED;

    public String value() {
        return name();
    }

    public static PokerGameState fromValue(String v) {
        return valueOf(v);
    }

}
