
package py.edu.uca.ws.pokergame;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "InvalidDescription", targetNamespace = "http://pokergame.ws.uca.edu.py")
public class InvalidDescriptionException
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private InvalidDescription faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public InvalidDescriptionException(String message, InvalidDescription faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public InvalidDescriptionException(String message, InvalidDescription faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: py.edu.uca.ws.pokergame.InvalidDescription
     */
    public InvalidDescription getFaultInfo() {
        return faultInfo;
    }

}
