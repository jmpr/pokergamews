package py.edu.uca.ws.tp.objects;

/**
 * Contiene la información de un poker game
 * 
 * @author jmpr
 *
 */
public class PokerGameInfo {

	private String identifier;

	private String owner;

	private String description;

	private PokerGameState state;

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public PokerGameState getState() {
		return state;
	}

	public void setState(PokerGameState state) {
		this.state = state;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
}
