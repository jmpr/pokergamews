package py.edu.uca.ws.tp.exceptions;


public class InvalidEstimation extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidEstimation() {
		super();
	}

	public InvalidEstimation(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidEstimation(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidEstimation(String message) {
		super(message);
	}

	public InvalidEstimation(Throwable cause) {
		super(cause);
	}

}
