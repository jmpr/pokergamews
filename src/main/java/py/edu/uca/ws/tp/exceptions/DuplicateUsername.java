package py.edu.uca.ws.tp.exceptions;


public class DuplicateUsername extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicateUsername() {
	}

	public DuplicateUsername(String message) {
		super(message);
	}

	public DuplicateUsername(Throwable cause) {
		super(cause);
	}

	public DuplicateUsername(String message, Throwable cause) {
		super(message, cause);
	}

	public DuplicateUsername(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
