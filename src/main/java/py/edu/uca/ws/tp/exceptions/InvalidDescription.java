package py.edu.uca.ws.tp.exceptions;


public class InvalidDescription extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidDescription() {
		super();
	}

	public InvalidDescription(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidDescription(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidDescription(String message) {
		super(message);
	}

	public InvalidDescription(Throwable cause) {
		super(cause);
	}

}
