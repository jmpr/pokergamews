package py.edu.uca.ws.tp.objects;

public class Estimation {

	private int value;

	private String justification;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getJustification() {
		return justification;
	}

	public void setJustification(String justification) {
		this.justification = justification;
	}

}
