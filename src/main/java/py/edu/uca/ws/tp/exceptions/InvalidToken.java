package py.edu.uca.ws.tp.exceptions;


public class InvalidToken extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidToken() {
	}

	public InvalidToken(String message) {
		super(message);
	}

	public InvalidToken(Throwable cause) {
		super(cause);
	}

	public InvalidToken(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidToken(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
