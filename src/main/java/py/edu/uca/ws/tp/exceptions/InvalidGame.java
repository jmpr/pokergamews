package py.edu.uca.ws.tp.exceptions;


public class InvalidGame extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidGame() {
		super();
	}

	public InvalidGame(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidGame(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidGame(String message) {
		super(message);
	}

	public InvalidGame(Throwable cause) {
		super(cause);
	}

}
