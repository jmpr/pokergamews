package py.edu.uca.ws.tp;

import py.edu.uca.ws.tp.exceptions.DuplicateUsername;
import py.edu.uca.ws.tp.exceptions.InvalidDescription;
import py.edu.uca.ws.tp.exceptions.InvalidEstimation;
import py.edu.uca.ws.tp.exceptions.InvalidGame;
import py.edu.uca.ws.tp.exceptions.InvalidGameStatus;
import py.edu.uca.ws.tp.exceptions.InvalidToken;
import py.edu.uca.ws.tp.exceptions.InvalidUsername;
import py.edu.uca.ws.tp.objects.Estimation;
import py.edu.uca.ws.tp.objects.PokerGameResult;
import py.edu.uca.ws.tp.objects.PokerGameToken;

//TODO Hacer los cambios correspondientes a esta clase para 
// que una instancia de esta clase pueda ser publicada como un
// endpoint SOAP
public class PokerGame {

	/**
	 * Este método debe ser invocado para iniciar un juego.
	 * 
	 * 
	 * @param username
	 *            usuario que será el owner del juego
	 * @param description
	 *            descripción del juego
	 * @return
	 * @throws InvalidUsername
	 *             Si el usuario no cumple con las restricciones establecidas en
	 *             el documento de definición del trabajo
	 * @throws InvalidDescription
	 *             Si la descripción no cumple con alguna de las restricciones
	 *             establecidas en el documento de definición del trabajo
	 */

	public PokerGameToken start(String username, String description) throws InvalidUsername, InvalidDescription {
		// TODO implementar de acuerdo a la documentación y los tests
		if (username == null || username.trim().isEmpty()) {
			throw new InvalidUsername();
		}
		return null;
	}

	/**
	 * Este método debe ser invocado con el token correcto para cerrar un juego
	 * 
	 * @param token
	 *            el token que permite administrar el juego
	 * @param identifier
	 *            El identificador del juego
	 * @return
	 * @throws InvalidToken
	 *             Si el token no es válido
	 * @throws InvalidGameStatus
	 *             Si el juego ya fue cerrado
	 */

	public PokerGameResult end(String token, String identifier) throws InvalidToken, InvalidGameStatus {
		// TODO implementar de acuerdo a la documentación y los tests
		System.out.println("end");
		return null;
	}

	/**
	 * Este método retorna el estado de un juego.
	 * 
	 * Si el token es null, debe retornar las estimaciones si el juego finalizó.
	 * Solo con el token se pueden ver los resultados (parciales) antes de que
	 * termine el juego.
	 *
	 * Si el token es válido, se deben retornar todas las estimaciones hechas
	 * hasta el momento.
	 * 
	 * @param token
	 *            null o un token válido
	 * @param identifier
	 * @return Resultado (parcial o final) del juego
	 * @throws InvalidToken
	 */
	public PokerGameResult status(String token, String identifier) throws InvalidToken {
		// TODO implementar de acuerdo a la documentación y los tests
		System.out.println("status");
		return null;
	}

	/**
	 * Este método es invocado para realizar una estimación
	 * 
	 * @param username
	 *            El usuario que va realizar la estimación
	 * @param identifier
	 *            El identificador del juego
	 * @param estimation
	 *            La estimación
	 * @return el valor de la estimación
	 * @throws InvalidUsername
	 *             Si el usuario no cumple con las restricciones establecidas en
	 *             el documento de definición del trabajo
	 * @throws DuplicateUsername
	 *             Si el usuario ya realizó una estimación
	 * @throws InvalidGameStatus
	 *             Si el juego ya finalizó
	 * @throws InvalidGame
	 *             Si el juego no existe
	 * @throws InvalidEstimation
	 *             Si la estimación no cumple con los parámetros establecidos en
	 *             el documento
	 */
	public int play(String username, String identifier, Estimation estimation) throws InvalidUsername, DuplicateUsername,
			InvalidGameStatus, InvalidGame, InvalidEstimation {
		// TODO implementar de acuerdo a la documentación y los tests
		System.out.println("play");
		return 0;
	}

}
