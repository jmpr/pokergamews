package py.edu.uca.ws.tp.objects;

public class PokerGameToken extends PokerGameInfo {

	private String token;

	/**
	 * El token para administrar el juego.
	 * 
	 * A través de un token se identifica un juego
	 * 
	 * @return
	 */
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
