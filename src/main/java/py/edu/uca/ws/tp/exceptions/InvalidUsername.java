package py.edu.uca.ws.tp.exceptions;


public class InvalidUsername extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidUsername() {
	}

	public InvalidUsername(String message) {
		super(message);
	}

	public InvalidUsername(Throwable cause) {
		super(cause);
	}

	public InvalidUsername(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidUsername(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
