package py.edu.uca.ws.tp.objects;

import java.util.Map;

/**
 * Información sobre el juego con resultados parciales o finales
 * 
 * @author jmpr
 *
 */
public class PokerGameResult extends PokerGameInfo {

	private Map<String, Estimation> estimations;

	/**
	 * Las estimaciones hechas por los usuarios.
	 * 
	 * El key es el usuario. Este map debe parmanecer ordenado en el orden en el
	 * que se hicieron las estimaciones
	 * 
	 * @return
	 */
	public Map<String, Estimation> getEstimations() {
		return estimations;
	}

	public void setEstimations(Map<String, Estimation> estimations) {
		this.estimations = estimations;
	}

}
