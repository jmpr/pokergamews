package py.edu.uca.ws.tp.exceptions;


public class InvalidGameStatus extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidGameStatus() {
		super();
	}

	public InvalidGameStatus(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidGameStatus(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidGameStatus(String message) {
		super(message);
	}

	public InvalidGameStatus(Throwable cause) {
		super(cause);
	}

}
